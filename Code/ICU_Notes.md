```python
import openai

openai.api_key = "enter your key here"
```


```python

def call_chatgpt_general(system_prompt, input_content, few_shot_examples=[]):
    messages = [{"role": "system", "content": system_prompt}]
    # Add few-shot examples to messages correctly
    for example in few_shot_examples:
        # Directly appending the example assuming it's formatted correctly
        messages.append({"role": "system", "content": example})
    # Add the user's prompt
    messages.append({"role": "user", "content": input_content})
    
    response = openai.ChatCompletion.create(
        model="gpt-4", 
        messages=messages,
        temperature=0.9,
        max_tokens=5000,
        top_p=0.95,
        frequency_penalty=0,
        presence_penalty=0,
        stop=None
    )
    
    return response['choices'][0]['message']['content']

# Define your few-shot example(s)
few_shot_examples = ["""
Subjective: Patient with worsening shock overnight requiring central line placement and norepinephrine to .14

Objective:
Vitals: 
HR: 110, BP: 80/55, Temp: 101.2F, SpO2: 95%
Labs:
CBC: Hgb: 13.8, WBC: 16.7, Plt 512
BMP: Na: 137, K: 4.2, Cl: 105, CO2: 24, BUN: 24, Cr: 1.73
Lactate: 4.2
Blood cultures: Gram negative rods
Urinalysis: Nitrate: Positive, Leukocyte esterase: positive, WBC: >50, RBC 0
Urine cultures: Gram negative rods

Intake/Output: In: 2578 mL/ Out: 1895 mL

CPOT:0
RASS: -1
CAM-ICU: negative

Imaging: 
Chest x-ray: Normal cardiac border, no infiltrates or airspace disease, no pleural effusions
CT Abdomen and pelvis w/ contrast: Right kidney abscess present in calyx of lower pole. No kidney stones. 

Assessment:
73 year old female with hyperlipidemia, and CAD s/p stent to RCA in 2016, history of recurrent urinary tract infection complicated by kidney stones, who presented to the hospital in septic shock from pyelonephritis.

Plan:
1.Septic shock secondary to gram negative rod bacteremia with pyelonephritis.Pyelonephritis
-Continue zosyn 4.5g Q8h
-S/p percutaneous nephrostomy placement with interventional radiology
-Continue Norepinephrine MAP goal >65
-F/u blood and urine cultures
-Start Hydrocortisone 50mg q6h and fludrocortisone 50mg daily
-Consider initiation of vasopressin if MAP continues to be less than 65

3.KDIGO stage 2 Acute kidney injury
-Strict I/Os with goal urine output of 0.5cc/kg/hr
- Attempt Lasix challenge today
-Avoid nephrotoxic agents

4. Hyperlipidemia: Hold losartan and amlodipine in setting of critical illness
5. CAD: Continue ASA, hold antihypertensives while in septic shock

Nutrition: Holding TF in setting of critical illness
Code: DNR/OTI
DVT Ppx: Subcutaneous heparin 5000 units q12h
GI Ppx: Pantoprazole 40mg IV daily
"""]


# Define your input
num_of_calls = 300 
system_prompt = "You are a critical care researcher who needs to generate daily progress notes (also known as SOAP notes) designed for multi-speciality ICU research dataset"
# Define your detailed step-by-step instructions for generating a SOAP note

input_content = """
Generate the SOAP note following these step-by-step instructions:
Step 1
Generate a random patient in the intensive care unit with a specific focus on a critical care type such as neurological and neurosurgical, medical, surgical, trauma, or cardiac, or cardiothoracic. The patient should have a primary diagnosis and may have secondary diagnoses and chronic comorbidities.

Step 2
Generate a daily SOAP note with the sections Subject, Objective, Assessment, and Plan has headers preceded by ##
Do not use any part of the example note data 

Step 3
The subjective section should only contain important events that occurred in the prior 24 hours

Step 4
The objective section should contain vital signs, ventilator settings, positive physical exam findings, laboratory data, imaging reports (chest xrays, ultrasound, CT scans). Also include input/output fluid data and pain, sedation, and delirium scores.

Step 5
The Assessment section should be a narrative summary of the patient’s initial presentation, comorbidities followed by the primary and secondary diagnoses.

Step 6
The plan section should contain primary and secondary diagnoses followed by the treatment plan underneath. Provide between two and seven diagnoses.
Be specific about the treatment plan and do not generalize.
Plan should be consistent with data in the subjective and objective sections.
Include nutrition plan.
Include DVT prophylaxis if applicable.
Include GI prophylaxis if applicable.
Include code status.

Step 7
Perform Steps 1-6 for SOAP notes that represent Medical ICU, Surgical ICU, Neuro ICU, Neurosurgery ICU, Trauma ICU, Cardiac Care Unit, and CT Surgery ICU

Step 8
Do not generate anything extra and stop after SOAP note is complete.
"""
output = []

target_csv_filename = "chatgpt_syn_notes.csv"

for _ in range(num_of_calls):
    chatgpt_response = call_chatgpt_general(system_prompt, input_content, few_shot_examples)
    output.append(chatgpt_response)
    
with open(target_csv_filename, "w", newline='') as outf:
    wr = csv.writer(outf)
    header = ["ChatGPT Output"]
    wr.writerow(header)
    for sample in output:
        wr.writerow([sample])

```
